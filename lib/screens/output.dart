import 'package:flutter/material.dart';

import '../samples/app_bar.dart';
import '../samples/bottom_nav.dart';
import '../samples/bottom_sheet.dart';
import '../samples/drawer.dart';
import '../samples/gallery.dart';
import '../samples/home_sample.dart';
import '../samples/login.dart';
import '../samples/sliver_appbar.dart';
import '../samples/splash.dart';
import '../samples/tabbar.dart';
import '../samples/todo.dart';
import '../widgets/buttons.dart';
import '../widgets/checkbox.dart';
import '../widgets/column.dart';
import '../widgets/container.dart';
import '../widgets/dropdown.dart';
import '../widgets/materialapp.dart';
import '../widgets/popupmenu.dart';
import '../widgets/row.dart';
import '../widgets/snackbar.dart';
import '../widgets/stack.dart';
import '../widgets/text.dart';
import 'home.dart';

class OutputWidget extends StatefulWidget {
  final Sample sample;

  OutputWidget({Key key, @required this.sample}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return OutputState();
  }
}

class OutputState extends State<OutputWidget> {
  Widget screenName;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(10),
        child: Scaffold(
          body: Center(child: loadPreview(context)),
        ));
  }

  Widget loadPreview(BuildContext context) {
    switch (widget.sample.id) {
      case 1:
        {
          screenName = SplashScreen();
          break;
        }
      case 2:
        {
          screenName = LoginScreen();
          break;
        }
      case 3:
        {
          screenName = TodoScreen();
          break;
        }
      case 4:
        {
          screenName = GalleryScreen();
          break;
        }
      case 5:
        {
          screenName = HomeSample();
          break;
        }
      case 6:
        {
          screenName = DrawerScreen();
        }
        break;
      case 7:
        {
          screenName = AppBarScreen();
        }
        break;
      case 8:
        {
          screenName = BottomNavigationScreen();
        }
        break;
      case 9:
        {
          screenName = SliverAppBarScreen();
        }
        break;
      case 10:
        {
          screenName = TabBarClass();
        }
        break;
      case 11:
        {
          screenName = ButtonsScreen();
        }
        break;
      case 12:
        {
          screenName = DropDownScreen();
        }
        break;
      case 13:
        {
          screenName = PopupMenuScreen();
        }
        break;
      case 14:
        {
          screenName = SnackBarScreen();
        }
        break;
      case 15:
        {
          screenName = TextScreen();
        }
        break;
      case 16:
        {
          screenName = CheckBoxScreen();
        }
        break;
      case 17:
        {
          screenName = RowScreen();
        }
        break;
      case 18:
        {
          screenName = ColumnScreen();
        }
        break;
      case 19:
        {
          screenName = StackScreen();
        }
        break;
      case 20:
        {
          screenName = ContainerScreen();
        }
        break;
      case 21:
        {
          screenName = MaterialAppScreen();
        }
        break;
      case 22:
        {
          screenName = BottomSheetScreen();
        }
        break;
    }
    return screenName;
  }
}
