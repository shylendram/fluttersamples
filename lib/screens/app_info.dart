import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';

class AppInfoScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Flutter Samples - App Info")),
        body: Container(
          color: Colors.blue,
          child: ListView(
            padding: const EdgeInsets.all(30),
            children: <Widget>[
              Image.asset('assets/images/flutter_icon.png',
                  width: 150, height: 80),
              SizedBox(height: 20),
              Text('Flutter Samples',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
              FutureBuilder<PackageInfo>(
                future: PackageInfo.fromPlatform(),
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.done:
                      return Text(
                        'Version: ${snapshot.data.version}',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 14),
                      );
                    default:
                      return const SizedBox();
                  }
                },
              ),
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Text(
                    ('This app is basically to learn Flutter Widgets and Samples with souce code. More samples will be updated.')),
              )
            ],
          ),
        ));
  }
}
