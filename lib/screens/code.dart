import 'package:flutter/material.dart';

import 'home.dart';

class CodeWidget extends StatelessWidget {
  final Sample sample;

  CodeWidget({Key key, @required this.sample}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Center(
            child: Padding(
      padding: const EdgeInsets.all(20),
      child: SelectableText(sample.sourceCode,
          cursorColor: Colors.blue,
          showCursor: true,
          toolbarOptions: ToolbarOptions(
              copy: true,
              selectAll: true,
              cut: false,
              paste: false
          ),
          style: TextStyle(color: Colors.black, fontStyle: FontStyle.normal)),
    )));
  }
}
